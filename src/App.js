import './App.css';
import MainApp from './components/main';

function App() {
  return (
    <div className="App">
      <MainApp />
    </div>
  );
}

export default App;
