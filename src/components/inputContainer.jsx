import React, { Component } from "react";
import "../styles/inputContainer.css";

export default class InputContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoData: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      todoData: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.newTodo(this.state.todoData);
    this.setState({
      todoData: "",
    });
  };

  render() {
    return (
      <form id="input-todo" onSubmit={this.handleSubmit}>
        <button id="submit-data" />
        <input
          type="text"
          id="todo-data"
          placeholder="Create a new todo..."
          value={this.state.todoData}
          onChange={this.handleChange}
        />
      </form>
    );
  }
}
