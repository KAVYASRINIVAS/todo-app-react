import React, { Component } from "react";
import "../styles/footer.css";

const FILTERS = [
  {
    label: "All",
    name: "all",
  },
  {
    label: "Active",
    name: "active",
  },
  {
    label: "Completed",
    name: "completed",
  },
];

class FooterContainer extends Component {
  render() {
    const activetodos = this.props.todos.filter(
      (todo) => !todo.complete
    ).length;
    return (
      <section className="counter-and-clear-operation">
        <span> {activetodos} Items left </span>
        {FILTERS.map((filter) => {
          return (
            <button
              key={filter.name}
              onClick={() => this.props.handleFilters(filter)}
            >
              {filter.label}
            </button>
          );
        })}

        <button id="clear-completed" onClick={this.props.clearCompleted}>
          Clear Completed
        </button>
      </section>
    );
  }
}

export default FooterContainer;
