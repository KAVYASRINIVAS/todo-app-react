import React, { Component } from "react";
import "../styles/todoContainer.css";

class TodoContainer extends Component {
  render() {
    return (
      <section className="list-holder">
        <div id="task-todo">
          {this.props.todoData.map((todo) => {
            return (
              <div key={todo.id} className="todoList">
                <input
                  type="checkbox"
                  className="todoCheckbox"
                  id={todo.id}
                  checked={todo.complete}
                  onClick={() => this.props.checkCompleted(todo.id)}
                  onChange={() => {}}
                />
                <span id={todo.id}>{todo.data}</span>
                <button
                  className="todoDelete"
                  onClick={() => this.props.deleteItem(todo.id)}
                ></button>
              </div>
            );
          })}
        </div>
      </section>
    );
  }
}

export default TodoContainer;
