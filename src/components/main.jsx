import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import HeaderSection from "./header";
import InputContainer from "./inputContainer";
import TodoContainer from "./todoContainer";
import FooterContainer from "./footerContainer";

import "../styles/main.css";

class MainApp extends Component {
  // Holds todo data
  state = {
    todos: [],
    activeFilter: "all",
  };

  // Inserts new Todo
  handleInputTodo = (text) => {
    if (text.length !== 0) {
      let todo = {
        data: text,
        id: uuidv4(),
        complete: false,
      };
      this.setState({
        todos: [todo, ...this.state.todos],
      });
    } else {
      alert("Please enter your todo!");
    }
  };

  // Complete todo
  handleComplete = (todoId) => {
    this.setState(
      {
        todos: this.state.todos.map((todo) => {
          if (todo.id === todoId) {
            return {
              ...todo,
              complete: !todo.complete,
            };
          }
          return todo;
        }),
      },
      () => console.log(this.state.todos)
    );
  };

  // Delete todo via cross button
  deleteTodo = (todoId) => {
    this.setState({
      todos: this.state.todos.filter((todo) => todo.id !== todoId),
    });
  };

  // Clear completed todos
  handleDeleteCompleted = () => {
    this.setState({
      todos: this.state.todos.filter((todo) => !todo.complete),
    });
  };

  getFilteredTodos = (filter) => {
    if (filter === "all") {
      return this.state.todos;
    } else if (filter === "active") {
      return this.state.todos.filter((todo) => !todo.complete);
    } else if (filter === "completed") {
      return this.state.todos.filter((todo) => todo.complete);
    } else {
      return [];
    }
  };

  handleFilter = (filter) => {
    this.setState({
      activeFilter: filter.name,
    });
  };

  render() {
    const todoData = this.getFilteredTodos(this.state.activeFilter);
    return (
      <section className="main">
        <HeaderSection />
        <InputContainer newTodo={this.handleInputTodo} />
        <TodoContainer
          todoData={todoData}
          checkCompleted={this.handleComplete}
          deleteItem={this.deleteTodo}
        />
        <FooterContainer
          todos={this.state.todos}
          handleFilters={this.handleFilter}
          clearCompleted={this.handleDeleteCompleted}
        />
      </section>
    );
  }
}

export default MainApp;
