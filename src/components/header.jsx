import React, { Component } from "react";
import "../styles/header.css";

class HeaderSection extends Component {
  state = {
    buttonName: "light-theme-button",
    sectionName: "light-container",
  };

  handleTheme = (event) => {
    let className = event.target.className;
    if (className === "light-theme-button") {
      event.target.className = "dark-theme-button";
      event.target.parentElement.className = "dark-container";
      event.target.parentElement.parentElement.style.backgroundColor =
        "hsl(235, 24%, 19%)";
    } else {
      event.target.className = "light-theme-button";
      event.target.parentElement.className = "light-container";
      event.target.parentElement.parentElement.style.backgroundColor =
        "rgb(237, 230, 230)";
    }
  };

  render() {
    return (
      <section className={this.state.sectionName}>
        <h1 className="title">TODO</h1>
        <button className={this.state.buttonName} onClick={this.handleTheme} />
      </section>
    );
  }
}

export default HeaderSection;
